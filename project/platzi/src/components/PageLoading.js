//react
import React from 'react';
//css
import './styles/PageLoading.css';
//components
import Loader from './Loader';

function PageLoading() {
  return (
    <div className="PageLoading">
      <Loader />
    </div>
  );
}

export default PageLoading;