//REACT
import React from 'react';
//CSS
import './styles/Loader.css';



class Loader extends React.Component {
    render ( ) {
        return(
            <div className="lds-grid">
                <div />
                <div />
                <div />
                <div />
                <div />
                <div />
                <div />
                <div />
                <div />
            </div>
        );
    }
}

export default Loader;