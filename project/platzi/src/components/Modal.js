import React from 'react';
import ReactDOM from 'react-dom';

import './styles/Modal.css';

function Modal(props) {
  //Si no esta abierto el modal entonces regresamos null y no hacemos nada.
  //si es true quiere decir que el modal esta abierto
  if (!props.isOpen) {
    return null;
  }

  return ReactDOM.createPortal(
    <div className="Modal">
      <div className="Modal__container">
        <button onClick={props.onClose} className="Modal__close-button">
          X
        </button>
          {/* a este child se le llama composición, va a permitirnos que un futuro va a ser un composición tecnica especifica */}
          {props.children}
      </div>
    </div>,
    document.getElementById('modal')
  );
}

export default Modal;