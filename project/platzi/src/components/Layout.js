import React from 'react';

//Librerias Irdn
import Navbar from './Navbar'
function Layout (props) {
    //children lo voy a leer de los props
    const children = props.children;
    return (
        //Para evitar divs innecesarios ponemos 
        <React.Fragment>
            <Navbar />
            {props.children}
        </React.Fragment>
    );
}

export default Layout;