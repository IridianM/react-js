//react
import React from 'react';
//css
import './styles/PageError.css'



function PageError (props) {
    return <div className="PageError"> {props.error.message} </div>
}

export default PageError;
