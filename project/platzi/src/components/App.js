//Librerias react
import React from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';
//Librerias Irdn.
import Layout from '../components/Layout';
import BadgeNew from '../pages/BadgeNew';
import Badges from '../pages/Badges';
import Home from '../pages/Home';
import BadgeEdit from '../pages/BadgeEdit';
import BadgeDetails from '../pages/BadgeDetails';
import NotFound from '../pages/NotFound';
import BadgeDetailsContainer from '../pages/BadgeDetailsContainer';
//Aqui no estoy utilizando clases por que es una simple page App
/*  Las aplicaciones que se trabajan en React son llamadas single page apps. Esto es posible gracias a React Router que es una librería Open Source.
    Multi Page Apps: Cada página implica una petición al servidor. La respuesta usualmente tiene todo el contenido de la página.
    Single Page Apps (SPA): Aplicaciones que cargan una sola página de HTML y cualquier actualización la hacen re-escribiendo el HTML que ya tenían.
    React Router (v4): Nos da las herramientas para poder hacer SPA fácilmente. Usaremos 4 componentes:

    BrowserRouter: es un componente que debe estar siempre lo más arriba de la aplicación. Todo lo que esté adentro funcionará como una SPA.
    Route: Cuando hay un match con el path, se hace render del component. El component va a recibir tres props: match, history, location.
    Switch: Dentro de Switch solamente van elementos de Route. Switch se asegura que solamente un Route se renderize y no toda la página completa, esto evita que toda la página se recargue innecesariamente.
    Link: Toma el lugar del elemento <a>, evita que se recargue la página completamente y actualiza la URL.
    exact :  dice que la ruta es exacta y no hay coincidenias
    a : este elemento hace que toda la pagina se recargue
    link : no recarga toda la página.
    */
function App () {
    return (
        //Browser Router
        <BrowserRouter>{/* Solo puede tener un hijo entonces debemos encerrarlo en un Div. */}
            <Layout>
                <Switch>{/* En vez de usar un div podemos usar un switch* */}
                    <Route exact path="/"  component={Home} />
                    <Route exact path="/badges"  component={Badges} />
                    <Route exact path="/badges/new" component={BadgeNew} />
                    <Route exact path="/badges/:badgeId" component={BadgeDetailsContainer} />
                    <Route exact path="/badges/:badgeId/edit" component={BadgeEdit} />
                    {/* Cuando ninguna ruta coincida mostraremos lo siguiente :  */}
                    <Route component={NotFound} />
                </Switch>
            </Layout>
        </BrowserRouter>

    );
}

export default App;