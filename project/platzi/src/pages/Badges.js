//Librerias React
import React from 'react';
import { Link } from 'react-router-dom';
//Librerias Irdn.
import './styles/Badges.css';
import confLogo from '../images/badge-header.svg';
import BadgesList from '../components/BadgesList';
import PageLoading from '../components/PageLoading';
import PageError from '../components/PageError';
import MiniLoader from '../components/MiniLoader';
import api from '../api';




class Badges extends React.Component {
  state = {
    loading: true,
    error : null,
    data: undefined
  };


  //El mejor lugar para llamar a una API es en el component did mount
  //Comienza la petición FetchData.
  componentDidMount () {
    this.fetchData();
    //Refrescar la pagina cada cierto tiempo 
    this.intervalId = setInterval(this.fetchData, 5000);
  }

  
  componentWillUnmount () {
    //Para detener el ntervalo que se había quedadoo funionando en la página anterior.
    clearInterval(this.intervalId);
  }


  fetchData = async () => {
    this.setState( { loading: true, error: null} )

    //Llamada a la API
    try {
      //llamada asincrona regresa una promesa
       const data = await api.badges.list();
       this.setState({ loading: false, data: data});
    }
    catch (error) {
        //En el caso de que hubiera un error
        this.setState({ loading: false, error: error})
    }
  }


  render() {
    //Mostrar elemento de cargando.
    if(this.state.loading === true && !this.state.data) {
      //Experiencia de Uusario.
      return <PageLoading />;
    }
    //Si hay algún error.
    if(this.state.error){
      //Experiencia de Uusario.
      //return `Error: ${this.state.error.message}`;
      return <PageError error={this.state.error} />
    }


    return (

      <React.Fragment>
        {/* Aqui estuviera el Navbar 
          <Navbar />
        */}
        <div className="Badges">
          <div className="Badges__hero">
            <div className="Badges__container">
              <img
                className="Badges_conf-logo"
                src={confLogo}
                alt="Conf Logo"
              />
            </div>
          </div>
        </div>
        {/* Button que redirige a otra página. */}
        <div className="Badges__container">
          <div className="Badges__buttons">
            <Link to="/badges/new" className="btn btn-primary">
              New Badge
            </Link>
          </div>

          <BadgesList badges={this.state.data} />
          {this.state.loading && <MiniLoader />}

        </div>
      </React.Fragment>
    );
  }
}

export default Badges;