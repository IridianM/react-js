import React from 'react';
import './styles/BadgeNew.css';
import header from '../images/badge-header.svg';
import Badge from '../components/Badge';
import BadgeForm from '../components/BadgeForm';
import PageLoading from '../components/PageLoading';
import api from '../api';


class BadgeNew extends React.Component {
  //Estado de nuestros campos, vacio.
  state = {
    //Aqui el loaging va a ser false y representa que estamos enviando los datos
    loading: false,
    error:null,
    form: {
      firstName: '',
      lastName: '',
      email: '',
      jobTitle: '',
      twitter: '',
    },
  };
  //Manejar los eventos.
  handleChange = e => {
    this.setState({
      form: {
        //dejamos caer todos los valores que había en this.state.form,
        ...this.state.form,
        //añadimos uno nuevo 
        [e.target.name]: e.target.value,
      },
    });
  };

  //Vamos a enviar datos con post.
  handleSubmit = async e => {
    //prevenimos que se envien datos cuándo se hace click en el navegador.
    e.preventDefault();
    this.setState( { loading: true, error: null } );
    //Comenzamos a hacer la llamada
    try{
      await api.badges.create(this.state.form);
      //detenemos el loading y los datos no los vamos a usar.
      this.setState( {loading: false} );
      //en caso de que lleguemos a esta linea.
      this.props.history.push('/badges');
    }catch (error) {
      this.setState({ loading: false, error: error })
    }
  }
  


  render() {
    if(this.state.loading) {
      return <PageLoading />
    }
    return (
      <React.Fragment>
        <div className="BadgeNew__hero">
          <img className="img-fluid" src={header} alt="Logo" />
        </div>

        <div className="container">
          <div className="row">
            <div className="col-6">
              <Badge
              //Antes
              //firstName="Richar Kaufman"
              //Después, le pasamos los datos de BadgeForm a el formulario del lado derecho.
                firstName={this.state.form.firstName  || 'FIRST_NAME'}
                lastName={this.state.form.lastName || 'LAST_NAME' }
                twitter={this.state.form.twitter || 'twitter' }
                jobTitle={this.state.form.jobTitle || 'JOB_TITLE' }
                email={this.state.form.email || 'email'}
                avatarUrl="http://iridian.artikunazo.mx/wp-content/uploads/2019/12/borlita-150x150.png"
              />
            </div>

            <div className="col-6">
            <h1>New Attendant</h1>
              <BadgeForm
                onChange={this.handleChange}
                onSubmit={this.handleSubmit}
                formValues={this.state.form}
                error={this.state.error}
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default BadgeNew;
